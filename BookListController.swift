
import UIKit

class BookListController: UITableViewController, UISearchBarDelegate {
    
    var bookArray = [BookSummary]()
    
    @IBOutlet weak var searchBar: UISearchBar!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = 88
        self.searchBar.delegate = self
        self.refreshData()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        println(searchText)
    }
    
    func refreshData() {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        Booklist.getBooks({(books: Array<BookSummary>) in
            for book in books {
                println(book.title)
            }
            self.bookArray = books
            dispatch_async(dispatch_get_main_queue(), {
                self.tableView.reloadData()
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            })
        })
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let destination = segue.destinationViewController as? DetailController {
            println("we are about to view a DetailController")
            let indexPath = self.tableView.indexPathForSelectedRow()
            if let row:Int = indexPath?.row {
                println("row \(row)")
                let bookID = bookArray[row] as BookSummary
                destination.bookID = bookID.id
            }
        }
    }
    
    //let indexPath = self.tableView.indexPathForSelectedRow()
    //if let row:Int = indexPath?.row {
    //let bookID = bookArray[row]
    //destination?.bookID = bookID.id
    //}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bookArray.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCellWithIdentifier("BookItem", forIndexPath: indexPath) as UITableViewCell
        
        let cell = tableView.dequeueReusableCellWithIdentifier("CustomBookItem", forIndexPath: indexPath) as CustomCellController
        
        if bookArray.count > 0 {
            let bookSummary = bookArray[indexPath.row]
            //cell.textLabel.text = bookSummary.title
            cell.title.text = bookSummary.title
            cell.detailTextLabel?.text = bookSummary.subTitle
            if let url = NSURL(string: bookSummary.thumbnail) {
                if let data = NSData(contentsOfURL: url) {
                    if let image:UIImage = UIImage(data: data) {
                        //cell.imageView.image = image
                        cell.imageView.image = image
                    }
                }
            }
        }
        return cell
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */
    
    

}
