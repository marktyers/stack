//
//  DetailController.swift
//  Stack
//
//  Created by Mark Tyers on 21/10/2014.
//  Copyright (c) 2014 Coventry University. All rights reserved.
//

import UIKit

class DetailController: UIViewController {
    
    var bookID:String?
    
    @IBOutlet weak var bookTitle: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.bookTitle.text = ""
        if let id = self.bookID {
            println("book id: \(id)")
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            Booklist.getBook(id, completion: {(result: BookDetail) in
                println(result.title)
                dispatch_async(dispatch_get_main_queue(), {
                    self.bookTitle.text = result.title
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                })
            })
        }
    }
    
    /*
    
    
    
    println(result.title)
    //println(result.subTitle)
    //println(result.description)
    //println(result.thumbnail)
    
    
    //self.bookTitle.alpha = 0.0
    })
*/

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
